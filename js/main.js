let xhr = new XMLHttpRequest();
xhr.open('GET', 'https://swapi.co/api/films/');
xhr.send();
xhr.onload = function () {
    if (xhr.status !== 200) {
        console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`);
    } else {
        const container = document.getElementById("film-list");
        const arrResult = JSON.parse(xhr.response);
        for (let i = 0; i < arrResult.results.length; i++) {
            const filmTitle = document.createElement('li');
            filmTitle.innerHTML = `Title: ${arrResult.results[i].title}; <br>
                Episode: ${arrResult.results[i].episode_id}; <br>
                Opening: ${arrResult.results[i].opening_crawl}<br>`;
            container.appendChild(filmTitle);
            const animOut = document.createElement("div");
            animOut.className = 'movingBallOut';
            filmTitle.appendChild(animOut);
            const animLine = document.createElement("div");
            animLine.className = 'movingBallLineG';
            animOut.appendChild(animLine);
            const animBall = document.createElement("div");
            animBall.className = 'movingBallG';
            animOut.appendChild(animBall);
            const showBtn = document.createElement('a');
            showBtn.className = 'show-btn';
            showBtn.setAttribute('data-episode', `${arrResult.results[i].episode_id}`);
            showBtn.innerText = 'Show characters';
            showBtn.addEventListener('click', show);
            filmTitle.appendChild(showBtn);
            const charactersContainer = document.createElement('ul');
            charactersContainer.className = 'container-char';
            filmTitle.appendChild(charactersContainer);
        }
    }
};

function show() {
    this.style.display = 'none';
    const anim = document.getElementsByClassName('movingBallOut');
    const containerChar = document.getElementsByClassName('container-char');
    const arrResult = JSON.parse(xhr.response);
    for (let i = 0; i < arrResult.results.length; i++) {
        for (let prop in arrResult.results[i]) {
            if (arrResult.results[i][prop] == this.dataset.episode) {
                for (let j = 0; j < arrResult.results[i].characters.length; j++) {
                    anim[i].style.display = 'block';
                    let req = new XMLHttpRequest();
                    req.open('GET', `${arrResult.results[i].characters[j]}`);
                    req.send();
                    req.onload = function () {
                        if (req.status !== 200) {
                            console.log(`Ошибка ${req.status}: ${req.statusText}`);
                        } else {
                            const arrName = JSON.parse(req.response);
                            const char = document.createElement("li");
                            char.textContent = arrName.name;
                            containerChar[i].appendChild(char);
                        }
                        anim[i].style.display = 'none';
                    };
                }
            }
        }
    }
}
